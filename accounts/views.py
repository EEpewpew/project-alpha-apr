from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LogInForm, SignUpForm

# Create your views here.


def login_view(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("list_projects")
        else:
            form.add_error(None, "Invalid username or password.")
    else:
        form = LogInForm()
    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("login")


def create_new_user(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                new_user = User(username=username)
                new_user.set_password(password)
                new_user.save()
                user = authenticate(username=username, password=password)
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error(None, "The passwords do not match.")
    else:
        form = SignUpForm()
        context = {
            "form": form,
        }
        return render(request, "accounts/signup.html", context)
